Wrapper project for notes & documentation.

```
git clone --recurse-submodules git@gitlab.com:charlesbrandt/charles-brandt.git
```

or

```
git submodule update --init --recursive
```

Grab `node_modules`

```
yarn
```

Run local dev server

```
yarn run docs:dev
```

Check for broken links / build errors

```
yarn run docs:build
```
